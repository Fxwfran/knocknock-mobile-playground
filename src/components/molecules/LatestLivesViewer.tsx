import { NetworkStatus } from '@apollo/client';
import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import {
  FlatList, StyleSheet, TouchableOpacity, View,
} from 'react-native';

import { Stream } from '../../generated/graphql';
import Text from '../atoms/Text';

interface Props {
  streams: Stream[],
  onRefetchData: () => void,
  refetchStatus: NetworkStatus
}

export const LatestLivesViewer = ({ streams, onRefetchData, refetchStatus }: Props) => {
  const [refreshing, setRefreshing] = useState<boolean>(false);

  const navigation = useNavigation();

  useEffect(() => {
    if ([NetworkStatus.loading, NetworkStatus.refetch].includes(refetchStatus)) return;
    setRefreshing(false);
    console.log({ streams });
    console.log({ filtered: streams?.filter((stream) => Boolean(stream.videoUrl)) });
  }, [refetchStatus]);

  const handleRefresh = () => {
    setRefreshing(true);
    onRefetchData();
  };

  return (
    <View style={styles.container}>
      <Text> List of streams </Text>
      <FlatList
        style={styles.list}
        data={streams?.filter((stream) => Boolean(stream.videoUrl))}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => navigation.navigate('VideoPlayback', { streamId: item._id })}>
            <View style={styles.item}>
              <Text style={styles.title}>{item._id}</Text>
            </View>
          </TouchableOpacity>
        )}
        keyExtractor={(item) => item._id}
        onRefresh={handleRefresh}
        refreshing={refreshing}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    width: '100%',
  },
  item: {
    marginHorizontal: 16,
    marginVertical: 8,
    padding: 20,
  },
  list: {
    flex: 1,
    width: '100%',
  },
  title: {
    fontSize: 32,
  },
});

export default LatestLivesViewer;
