import { ApolloProvider } from '@apollo/client';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import client from './apollo/client';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';

const App = () => {
  const colorScheme = useColorScheme();

  return (
    <ApolloProvider client={client}>
      <SafeAreaProvider>
        <Navigation colorScheme={colorScheme} />
      </SafeAreaProvider>
    </ApolloProvider>
  );
};

export default App;
