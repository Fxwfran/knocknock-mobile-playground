// eslint-disable-next-line import/no-unresolved
import { AGORA_APP_ID } from '@env';
import { useEffect, useRef, useState } from 'react';
import { Platform } from 'react-native';
import RtcEngine, {
  ChannelProfile,
  ClientRole,
  DegradationPreference, VideoOutputOrientationMode,
  VideoRemoteState,
} from 'react-native-agora';

import requestCameraAndAudioPermission from '../components/Permissions.android';

interface IStream {
  agoraEngine: RtcEngine | undefined,
  hasJoined: boolean,
  peerIds: Array<number>,
  broadcasterVideoState: VideoRemoteState
}

const useStream = (isBroadcast: boolean, channelId: string): IStream => {
  const agoraEngine = useRef<RtcEngine>();
  const [hasJoined, setHasJoined] = useState<boolean>(false);
  const [peerIds, setPeerIds] = useState<Array<number>>([]);
  const [broadcasterVideoState, setBroadcasterVideoState] = useState<VideoRemoteState>(VideoRemoteState.Decoding);

  const joinChannel = async () => {
    console.log(`joining channel ${channelId}`);
    try {
      await agoraEngine.current?.setClientRole(
        isBroadcast ? ClientRole.Broadcaster : ClientRole.Audience,
      );
      await agoraEngine.current?.joinChannel(
        null,
        channelId,
        null,
        isBroadcast ? 1337 : 0,
      );
    } catch (e) {
      console.error('Error joining channel', e);
    }
  };

  useEffect(() => {
    const initializeEngine = async () => {
      try {
        if (Platform.OS === 'android') await requestCameraAndAudioPermission();

        if (!AGORA_APP_ID) {
          console.error('The AGORA_APP_ID env var is missing. Check your .env file.');
          return;
        }

        agoraEngine.current = await RtcEngine.create(AGORA_APP_ID);
        await agoraEngine.current.enableVideo();
        await agoraEngine.current.setChannelProfile(
          ChannelProfile.LiveBroadcasting,
        );
        await agoraEngine.current.setVideoEncoderConfiguration({
          dimensions: {
            height: 1280,
            width: 720,
          },
          bitrate: 3420,
          minFrameRate: 15,
          frameRate: 30,
          degradationPrefer: DegradationPreference.MaintainQuality,
          orientationMode: VideoOutputOrientationMode.FixedPortrait,
        });

        agoraEngine.current.addListener('Warning', (warn) => {
          console.log('Warning', warn);
        });

        agoraEngine.current.addListener('Error', (err) => {
          console.log('Error', err);
        });

        agoraEngine.current.addListener('UserJoined', (uid, elapsed) => {
          console.log('UserJoined', uid, elapsed);
          // If new user
          if (peerIds.indexOf(uid) === -1) {
            setPeerIds((prevIds) => [...prevIds, uid]);
          }
        });

        agoraEngine.current.addListener('UserOffline', (uid, reason) => {
          console.log('UserOffline', uid, reason);
          setPeerIds((prevIds) => prevIds.filter((id) => id !== uid));
        });

        // If Local user joins RTC channel
        agoraEngine.current.addListener(
          'JoinChannelSuccess',
          (channel, uid, elapsed) => {
            console.log('JoinChannelSuccess', channel, uid, elapsed);
            setHasJoined(true);
          },
        );

        agoraEngine.current.addListener(
          'RemoteVideoStateChanged',
          (uid, state) => {
            if (uid === 1337) setBroadcasterVideoState(state);
          },
        );

        console.log('Agora Engine init OK');
      } catch (e) {
        console.error('Error initializing Agora Engine', e);
      }
    };
    initializeEngine().then(joinChannel);

    return () => {
      agoraEngine.current?.leaveChannel();
      agoraEngine.current?.destroy();
    };
  }, []);

  return {
    agoraEngine: agoraEngine.current,
    hasJoined,
    peerIds,
    broadcasterVideoState,

  };
};

export default useStream;
