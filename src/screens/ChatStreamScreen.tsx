import { RouteProp } from '@react-navigation/native';
import React from 'react';
import { Dimensions, StyleSheet } from 'react-native';

import CommentListing from '../components/organisms/CommentListing';
import View from '../components/organisms/View';
import { RootStackParamList } from '../types';

const dimensions = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: dimensions.width,
  },
});

type ChatStreamScreenRouteProp = RouteProp<RootStackParamList, 'ChatStream'>;

type Props = {
  route: ChatStreamScreenRouteProp
};

const ChatStreamScreen = ({ route }: Props) => {
  console.log({ route });
  return (
    <View style={styles.container}>
      <CommentListing streamId={route.params.streamId}/>
    </View>
  );
};

export default ChatStreamScreen;
