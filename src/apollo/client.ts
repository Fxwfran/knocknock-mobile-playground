import {
  ApolloClient, HttpLink, InMemoryCache, split,
} from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import { WebSocketLink } from '@apollo/client/link/ws';
import { getMainDefinition } from '@apollo/client/utilities';
import { APOLLO_URI } from '@env';

import { accessTokenVar } from './state';

if (!APOLLO_URI) {
  throw new Error('The APOLLO_URI env var is missing. Check your .env file.');
} else {
  console.log(`Using server from ${APOLLO_URI}`);
}

const httpLink = new HttpLink({
  uri: APOLLO_URI,
});

const wsLink = new WebSocketLink({
  uri: APOLLO_URI.replace('http', 'ws'),
  options: {
    reconnect: true,
  },
});

const splitLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === 'OperationDefinition'
      && definition.operation === 'subscription'
    );
  },
  wsLink,
  httpLink,
);

const authLink = setContext(async (_, { headers }) => ({
  headers: {
    ...headers,
    Authorization: accessTokenVar(),
  },
}));

const cache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        isLoggedIn: {
          read: () => accessTokenVar(),
        },
      },
    },
  },
});

const client = new ApolloClient({
  link: authLink.concat(splitLink),
  cache,
  connectToDevTools: true,
});

export default client;
