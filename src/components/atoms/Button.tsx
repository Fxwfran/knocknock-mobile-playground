import React from 'react';
import {
  StyleProp,
  StyleSheet,
  Text,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';

import Colors from '../../constants/Colors';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: Colors.primary,
    borderRadius: 6,
    justifyContent: 'center',
    paddingHorizontal: 91,
    paddingVertical: 14,
  },
  text: {
    color: Colors.dark.text,
    fontSize: 16,
    fontWeight: '500',
  },
});

type Props = {
  onPress?: () => void,
  title: string,
  style?: StyleProp<any>,
  fullWidth?: boolean,
  compact?: boolean,
} & TouchableOpacity['props'];

const Button = ({
  onPress, title, style, fullWidth, compact, ...otherProps
}: Props) => (
  <TouchableOpacity
    activeOpacity={0.8}
    onPress={onPress}
    style={[style,
      fullWidth && { alignSelf: 'stretch' },
    ]}
    {...otherProps}
  >
    <LinearGradient
      start={{ x: 0, y: 0 }}
      end={{ x: -4.69, y: 3 }}
      colors={['#FE5971', '#CA2047']}
      style={[styles.container,
        compact && { paddingHorizontal: 10, paddingVertical: 10 },
      ]}
    >
      <Text style={styles.text}>{title}</Text>
    </LinearGradient>
  </TouchableOpacity>
);

export default Button;
