import {
  gql, MutationHookOptions, QueryHookOptions, SubscriptionHookOptions, useMutation, useQuery, useSubscription,
} from '@apollo/client';

import { MutationStreamCommentArgs, StreamComment, StreamCommentFilter } from '../../generated/graphql';
import { GenericFilter } from '../types';

interface IAddStreamComment { streamComment: StreamComment }
export const useAddStreamComment = (options?: MutationHookOptions<IAddStreamComment>) => useMutation<IAddStreamComment, MutationStreamCommentArgs>(gql`
  mutation AddStreamComment($set: StreamCommentUpdate) {
    streamComment(set: $set) {
      _id
      messageBody
    }
  } 
`, options);

interface IGetStreamComments { streamComments: StreamComment[] }
export const useGetStreamComments = (options?: QueryHookOptions<IGetStreamComments, GenericFilter<StreamCommentFilter>>) => useQuery<IGetStreamComments>(gql`
  query GetStreamComments($filter: StreamCommentFilter) {
    streamComments(filter: $filter) {
      _id
      messageBody
    }
  }
`, options);

interface ISubscribeStreamComments { streamComments: StreamComment }
export const useSubscribeStreamComments = (options?: SubscriptionHookOptions<ISubscribeStreamComments>) => useSubscription<ISubscribeStreamComments>(gql`
  subscription SubscribeStreamComments($streamId: String) {
    streamComments(filter: { streamId: $streamId }) {
      _id
      messageBody
    }
  }
`, options);
