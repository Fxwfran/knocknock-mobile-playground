import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { StyleSheet, TextInput } from 'react-native';

import Button from '../atoms/Button';
import View from './View';

const styles = StyleSheet.create({
  bottomSpace: {
    marginBottom: 10,
  },
  container: {
    alignItems: 'stretch',
    flex: 1,
    justifyContent: 'center',
  },
  topSpace: {
    marginTop: 15,
  },
});

const ChatRoomIntro = () => {
  const navigation = useNavigation();
  const [streamId, setStreamId] = useState('');

  const handleChatRoom = () => {
    navigation.navigate('ChatStream', { streamId });
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.bottomSpace}
        value={streamId}
        onChangeText={setStreamId}
      />

      <Button
        style={styles.topSpace}
        title="Enter Char"
        onPress={handleChatRoom}
      />
    </View>
  );
};

export default ChatRoomIntro;
