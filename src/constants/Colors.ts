const tintColorLight = '#2f95dc';
const tintColorDark = '#fff';
const linksColor = '#2e78b7';

export default {
  transparent: 'transparent',
  primary: '#FE5971',
  black: '#222222',
  blackOpacity: 'rgba(34,34,34,0.25)',
  white: '#ffffff',
  light: {
    text: '#000000',
    background: '#ffffff',
    tint: tintColorLight,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight,
    linksColor,
  },
  dark: {
    text: '#ffffff',
    background: '#000000',
    tint: tintColorDark,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorDark,
    linksColor,
  },
};
