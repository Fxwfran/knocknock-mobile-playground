import {
  DarkTheme,
  DefaultTheme,
  NavigationContainer,
} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { ColorSchemeName } from 'react-native';

import GetPhoneCodeScreen from '../screens/auth/GetPhoneCodeScreen';
import LoginScreen from '../screens/auth/LoginScreen';
import ChannelSelectionScreen from '../screens/ChannelSelectionScreen';
import ChatStreamScreen from '../screens/ChatStreamScreen';
import LatestLives from '../screens/LatestLives';
import LiveVideoScreen from '../screens/LiveVideoScreen';
import LoginChatScreen from '../screens/LoginChatScreen';
import NotFoundScreen from '../screens/NotFoundScreen';
import ScreenSelector from '../screens/ScreenSelector';
import VideoPlaybackScreen from '../screens/VideoPlaybackScreen';
import { RootStackParamList } from '../types';
// A root stack navigator is often used for displaying modals on top of all other content
// Read more here: https://reactnavigation.org/docs/modal
const Stack = createStackNavigator<RootStackParamList>();

const RootNavigator = () => (
  <Stack.Navigator screenOptions={{ headerShown: false }}>
    <Stack.Screen name="ScreenSelector" component={ScreenSelector} />
    <Stack.Screen name="ChatStream" component={ChatStreamScreen}/>
    <Stack.Screen name="LatestLives" component={LatestLives} />
    <Stack.Screen name="GetPhoneCode" component={GetPhoneCodeScreen} />
    <Stack.Screen name="Login" component={LoginScreen} />
    <Stack.Screen name="ChannelSelection" component={ChannelSelectionScreen} />
    <Stack.Screen name="LiveVideo" component={LiveVideoScreen} />
    <Stack.Screen name="ChatIntroRoom" component={LoginChatScreen}/>
    <Stack.Screen name="VideoPlayback" component={VideoPlaybackScreen}/>
    <Stack.Screen
      name="NotFound"
      component={NotFoundScreen}
      options={{ title: 'Oops!' }}
    />
  </Stack.Navigator>
);

// If you are not familiar with React Navigation, we recommend going through the
// "Fundamentals" guide: https://reactnavigation.org/docs/getting-started
const Navigation = ({ colorScheme }: { colorScheme: ColorSchemeName }) => (
  <NavigationContainer
    theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}
  >
    <RootNavigator />
  </NavigationContainer>
);

export default Navigation;
