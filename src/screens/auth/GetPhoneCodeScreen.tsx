import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import {
  StyleSheet,
} from 'react-native';

import { useImperativeGetPhoneCode } from '../../apollo/hooks/user';
import Button from '../../components/atoms/Button';
import TextInput from '../../components/atoms/TextInput';
import View from '../../components/organisms/View';

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  phoneInput: {
    height: 50,
    marginBottom: 20,
    marginTop: 100,
  },
});

const GetPhoneCodeScreen = () => {
  const [phoneNumber, setPhoneNumber] = useState('');
  const getPhoneCode = useImperativeGetPhoneCode();
  const navigation = useNavigation();

  const submit = async () => {
    await getPhoneCode({ phoneNumber });
    navigation.navigate('Login', { phoneNumber });
  };
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.phoneInput}
        value={phoneNumber}
        textContentType="telephoneNumber"
        dataDetectorTypes="phoneNumber"
        keyboardType="phone-pad"
        onChangeText={(text) => setPhoneNumber(text.replaceAll(' ', '')) }
      />
      <Button onPress={submit} title="Send code"></Button>
    </View>
  );
};

export default GetPhoneCodeScreen;
