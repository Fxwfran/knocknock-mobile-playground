import { gql } from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};


export type DirectChat = {
  __typename?: 'DirectChat';
  _id: Scalars['String'];
  deleted?: Maybe<Scalars['Boolean']>;
  lastMessage?: Maybe<DirectMessage>;
  members?: Maybe<Array<Maybe<User>>>;
  messages?: Maybe<Array<Maybe<DirectMessage>>>;
};

export type DirectChatFilter = {
  _id?: Maybe<Scalars['String']>;
  deleted?: Maybe<Scalars['Boolean']>;
};

export type Query = {
  __typename?: 'Query';
  accessToken?: Maybe<Scalars['String']>;
  directChat?: Maybe<DirectChat>;
  directChats?: Maybe<Array<Maybe<DirectChat>>>;
  directMessage?: Maybe<DirectMessage>;
  directMessages?: Maybe<Array<Maybe<DirectMessage>>>;
  isLoggedIn?: Maybe<Scalars['String']>;
  phoneCode?: Maybe<Scalars['String']>;
  product?: Maybe<Product>;
  products?: Maybe<Array<Maybe<Product>>>;
  status?: Maybe<ApiStatus>;
  store?: Maybe<Store>;
  stores?: Maybe<Array<Maybe<Store>>>;
  stream?: Maybe<Stream>;
  streamComment?: Maybe<StreamComment>;
  streamComments?: Maybe<Array<Maybe<StreamComment>>>;
  streams?: Maybe<Array<Maybe<Stream>>>;
  theme?: Maybe<StreamTheme>;
  themes?: Maybe<Array<Maybe<StreamTheme>>>;
  user?: Maybe<User>;
  users?: Maybe<Array<Maybe<User>>>;
};


export type QueryAccessTokenArgs = {
  filter?: Maybe<LoginFilter>;
};


export type QueryDirectChatArgs = {
  filter?: Maybe<DirectChatFilter>;
};


export type QueryDirectChatsArgs = {
  filter?: Maybe<DirectChatFilter>;
};


export type QueryDirectMessageArgs = {
  filter?: Maybe<DirectMessageFilter>;
};


export type QueryDirectMessagesArgs = {
  filter?: Maybe<DirectMessageFilter>;
};


export type QueryPhoneCodeArgs = {
  phoneNumber?: Maybe<Scalars['String']>;
};


export type QueryProductArgs = {
  filter?: Maybe<ProductFilter>;
};


export type QueryProductsArgs = {
  filter?: Maybe<ProductFilter>;
};


export type QueryStoreArgs = {
  filter?: Maybe<StoreFilter>;
};


export type QueryStoresArgs = {
  filter?: Maybe<StoreFilter>;
};


export type QueryStreamArgs = {
  filter?: Maybe<StreamFilter>;
};


export type QueryStreamCommentArgs = {
  filter?: Maybe<StreamCommentFilter>;
};


export type QueryStreamCommentsArgs = {
  filter?: Maybe<StreamCommentFilter>;
};


export type QueryStreamsArgs = {
  filter?: Maybe<StreamFilter>;
};


export type QueryThemeArgs = {
  filter?: Maybe<StreamThemeFilter>;
};


export type QueryThemesArgs = {
  filter?: Maybe<StreamThemeFilter>;
};


export type QueryUserArgs = {
  filter?: Maybe<UserFilter>;
};


export type QueryUsersArgs = {
  filter?: Maybe<UserFilter>;
};

export type DirectMessage = {
  __typename?: 'DirectMessage';
  _id: Scalars['String'];
  deleted?: Maybe<Scalars['Boolean']>;
  readAt?: Maybe<Scalars['String']>;
  sentAt?: Maybe<Scalars['String']>;
  messageBody?: Maybe<Scalars['String']>;
  conversation?: Maybe<DirectChat>;
  sender?: Maybe<User>;
  receiver?: Maybe<User>;
};

export type DirectMessageFilter = {
  _id?: Maybe<Scalars['String']>;
  deleted?: Maybe<Scalars['Boolean']>;
};

export type DirectMessageUpdate = {
  deleted?: Maybe<Scalars['Boolean']>;
  readAt?: Maybe<Scalars['Boolean']>;
  sentAt?: Maybe<Scalars['Boolean']>;
  messageBody?: Maybe<Scalars['Boolean']>;
  conversationId?: Maybe<Scalars['String']>;
  receiverId?: Maybe<Scalars['String']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  directMessage?: Maybe<DirectMessage>;
  product?: Maybe<Product>;
  store?: Maybe<Store>;
  stream?: Maybe<Stream>;
  streamComment?: Maybe<StreamComment>;
  theme?: Maybe<StreamTheme>;
  user?: Maybe<User>;
};


export type MutationDirectMessageArgs = {
  filter?: Maybe<DirectMessageFilter>;
  set?: Maybe<DirectMessageUpdate>;
};


export type MutationProductArgs = {
  filter?: Maybe<ProductFilter>;
  set?: Maybe<ProductUpdate>;
};


export type MutationStoreArgs = {
  filter?: Maybe<StoreFilter>;
  set?: Maybe<StoreUpdate>;
};


export type MutationStreamArgs = {
  filter?: Maybe<StreamFilter>;
  set?: Maybe<StreamUpdate>;
};


export type MutationStreamCommentArgs = {
  filter?: Maybe<StreamCommentFilter>;
  set?: Maybe<StreamCommentUpdate>;
};


export type MutationThemeArgs = {
  filter?: Maybe<StreamThemeFilter>;
  set?: Maybe<StreamThemeUpdate>;
};


export type MutationUserArgs = {
  filter?: Maybe<UserFilter>;
  set?: Maybe<UserUpdate>;
};

export type Subscription = {
  __typename?: 'Subscription';
  directMessage?: Maybe<DirectMessage>;
  products?: Maybe<Product>;
  streamStarts?: Maybe<Stream>;
  streamEnds?: Maybe<Stream>;
  streamComments?: Maybe<StreamComment>;
};


export type SubscriptionStreamCommentsArgs = {
  filter?: Maybe<StreamCommentFilter>;
};

export type Product = {
  __typename?: 'Product';
  _id: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  description?: Maybe<Scalars['String']>;
  thumbnailUrl?: Maybe<Scalars['String']>;
  pinnedAt?: Maybe<Scalars['String']>;
  deleted?: Maybe<Scalars['Boolean']>;
  stream?: Maybe<Stream>;
  retailer?: Maybe<User>;
};

export type GreaterThanCondition = {
  gte?: Maybe<Scalars['Int']>;
};

export type ProductFilter = {
  _id?: Maybe<Scalars['String']>;
  price?: Maybe<GreaterThanCondition>;
  deleted?: Maybe<Scalars['Boolean']>;
};

export type ProductUpdate = {
  name?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  description?: Maybe<Scalars['String']>;
  thumbnailUrl?: Maybe<Scalars['String']>;
  pinnedAt?: Maybe<Scalars['String']>;
  deleted?: Maybe<Scalars['Boolean']>;
  streamId?: Maybe<Scalars['String']>;
};

export type DbConnections = {
  __typename?: 'DbConnections';
  current?: Maybe<Scalars['Int']>;
  available?: Maybe<Scalars['Int']>;
  totalCreated?: Maybe<Scalars['Int']>;
  active?: Maybe<Scalars['Int']>;
  exhaustIsMaster?: Maybe<Scalars['Int']>;
  awaitingTopologyChanges?: Maybe<Scalars['Int']>;
};

export type DbStatus = {
  __typename?: 'DbStatus';
  connections?: Maybe<DbConnections>;
};

export type ApiStatus = {
  __typename?: 'ApiStatus';
  healthy?: Maybe<Scalars['Boolean']>;
  environment?: Maybe<Scalars['String']>;
  dbStatus?: Maybe<DbStatus>;
};

export type Store = {
  __typename?: 'Store';
  _id: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  stickers?: Maybe<Array<Maybe<Scalars['Int']>>>;
  avatar?: Maybe<Scalars['String']>;
  tags?: Maybe<Array<Maybe<Scalars['String']>>>;
  viewersAmount?: Maybe<Scalars['Int']>;
  salesAmount?: Maybe<Scalars['Int']>;
  openingTimes?: Maybe<OpeningTimes>;
  deleted?: Maybe<Scalars['Boolean']>;
  owner?: Maybe<User>;
};

export type OpeningTimes = {
  __typename?: 'OpeningTimes';
  monday?: Maybe<Scalars['String']>;
  tuesday?: Maybe<Scalars['String']>;
  wednesday?: Maybe<Scalars['String']>;
  thursday?: Maybe<Scalars['String']>;
  friday?: Maybe<Scalars['String']>;
  saturday?: Maybe<Scalars['String']>;
  sunday?: Maybe<Scalars['String']>;
};

export type StoreFilter = {
  _id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  deleted?: Maybe<Scalars['Boolean']>;
};

export type StoreUpdate = {
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  stickers?: Maybe<Array<Maybe<Scalars['Int']>>>;
  avatar?: Maybe<Scalars['String']>;
  tags?: Maybe<Scalars['String']>;
  openingTimes?: Maybe<OpeningTimesInput>;
  deleted?: Maybe<Scalars['Boolean']>;
  ownerId?: Maybe<Scalars['String']>;
};

export type OpeningTimesInput = {
  monday?: Maybe<Scalars['String']>;
  tuesday?: Maybe<Scalars['String']>;
  wednesday?: Maybe<Scalars['String']>;
  thursday?: Maybe<Scalars['String']>;
  friday?: Maybe<Scalars['String']>;
  saturday?: Maybe<Scalars['String']>;
  sunday?: Maybe<Scalars['String']>;
};

export type Stream = {
  __typename?: 'Stream';
  _id: Scalars['String'];
  title?: Maybe<Scalars['String']>;
  startedAt?: Maybe<Scalars['String']>;
  finishedAt?: Maybe<Scalars['String']>;
  thumbnailUrl?: Maybe<Scalars['String']>;
  videoUrl?: Maybe<Scalars['String']>;
  deleted?: Maybe<Scalars['Boolean']>;
  channelName?: Maybe<Scalars['String']>;
  comments?: Maybe<Array<Maybe<StreamComment>>>;
  retailer?: Maybe<User>;
  theme?: Maybe<StreamTheme>;
  products?: Maybe<Array<Maybe<Product>>>;
};

export type StreamFilter = {
  _id?: Maybe<Scalars['String']>;
  channelName?: Maybe<Scalars['String']>;
  finished?: Maybe<Scalars['Boolean']>;
  deleted?: Maybe<Scalars['Boolean']>;
};

export type StreamUpdate = {
  title?: Maybe<Scalars['String']>;
  startedAt?: Maybe<Scalars['String']>;
  finishedAt?: Maybe<Scalars['String']>;
  thumbnailUrl?: Maybe<Scalars['String']>;
  videoUrl?: Maybe<Scalars['String']>;
  deleted?: Maybe<Scalars['Boolean']>;
  channelName?: Maybe<Scalars['String']>;
  themeId?: Maybe<Scalars['String']>;
};

export type StreamComment = {
  __typename?: 'StreamComment';
  _id: Scalars['String'];
  messageBody?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  deleted?: Maybe<Scalars['Boolean']>;
  sender?: Maybe<User>;
  stream?: Maybe<Stream>;
};

export type StreamCommentFilter = {
  _id?: Maybe<Scalars['String']>;
  streamId?: Maybe<Scalars['String']>;
  deleted?: Maybe<Scalars['Boolean']>;
};

export type StreamCommentUpdate = {
  messageBody?: Maybe<Scalars['String']>;
  deleted?: Maybe<Scalars['Boolean']>;
  streamId?: Maybe<Scalars['String']>;
};

export type StreamTheme = {
  __typename?: 'StreamTheme';
  _id: Scalars['String'];
  name: Scalars['String'];
  icon: Scalars['String'];
  deleted?: Maybe<Scalars['Boolean']>;
};

export type StreamThemeFilter = {
  _id?: Maybe<Scalars['String']>;
};

export type StreamThemeUpdate = {
  name?: Maybe<Scalars['String']>;
  icon?: Maybe<Scalars['String']>;
  deleted?: Maybe<Scalars['Boolean']>;
};

export enum UserCategory {
  Consumer = 'CONSUMER',
  RetailRequested = 'RETAIL_REQUESTED',
  RetailApproved = 'RETAIL_APPROVED',
  RetailPublished = 'RETAIL_PUBLISHED',
  Admin = 'ADMIN'
}

export type User = {
  __typename?: 'User';
  _id: Scalars['String'];
  phoneNumber?: Maybe<Scalars['String']>;
  displayedName?: Maybe<Scalars['String']>;
  messagesSent?: Maybe<Scalars['Int']>;
  userCategory?: Maybe<UserCategory>;
  createdAt?: Maybe<Scalars['String']>;
  deleted?: Maybe<Scalars['Boolean']>;
  store?: Maybe<Store>;
};

export type UserFilter = {
  _id?: Maybe<Scalars['String']>;
  displayedName?: Maybe<Scalars['String']>;
  userCategory?: Maybe<UserCategory>;
  deleted?: Maybe<Scalars['Boolean']>;
};

export type UserUpdate = {
  phoneNumber?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  displayedName?: Maybe<Scalars['String']>;
  userCategory?: Maybe<UserCategory>;
  deleted?: Maybe<Scalars['Boolean']>;
};

export type LoginFilter = {
  phoneNumber: Scalars['String'];
  phoneCode: Scalars['Int'];
};
