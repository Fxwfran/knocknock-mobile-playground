import {
  gql,
  LazyQueryHookOptions,
} from '@apollo/client';

import { QueryAccessTokenArgs, QueryPhoneCodeArgs } from '../../generated/graphql';
import { useImperativeQuery } from '../utils';

interface IGetPhoneCode {
  phoneCode: string
}

interface IGetAccessToken {
  accessToken: string
}

export const useImperativeGetPhoneCode = (options?: LazyQueryHookOptions<IGetPhoneCode, QueryPhoneCodeArgs>) => useImperativeQuery<IGetPhoneCode, QueryPhoneCodeArgs>(gql`
  query ImperativeGetPhoneCode($phoneNumber: String) {
      phoneCode(phoneNumber: $phoneNumber)
  }
`, options);

export const useImperativeGetAccessToken = (options?: LazyQueryHookOptions<IGetAccessToken, QueryAccessTokenArgs>) => useImperativeQuery<IGetAccessToken, QueryAccessTokenArgs>(gql`
  query ImperativeGetAccessToken($filter: LoginFilter) {
      accessToken(filter: $filter)
  }
`, options);
