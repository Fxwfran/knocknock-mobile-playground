import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { ActivityIndicator, StyleSheet } from 'react-native';
import {
  RtcLocalView, RtcRemoteView, VideoRemoteState, VideoRenderMode,
} from 'react-native-agora';
import KeepAwake from 'react-native-keep-awake';

import { useAddStream, useImperativeGetStream, useStopStream } from '../../apollo/hooks/streams';
import Colors from '../../constants/Colors';
import Layout from '../../constants/Layout';
import useStream from '../../hooks/useStream';
import Button from '../atoms/Button';
import Text from '../atoms/Text';
import CommentListing from './CommentListing';
import View from './View';

const videoStateMessage = (state: VideoRemoteState) => {
  switch (state) {
  case VideoRemoteState.Stopped:
    return 'Video turned off by Host';
  case VideoRemoteState.Frozen:
    return 'Connection Issue, Please Wait';
  case VideoRemoteState.Failed:
    return 'Network Error';
  default:
    return null;
  }
};

const styles = StyleSheet.create({
  broadcasterVideoStateMessage: {
    alignItems: 'center',
    backgroundColor: Colors.black,
    flex: 1,
    justifyContent: 'center',
    width: '100%',
  },
  broadcasterVideoStateMessageText: {
    color: Colors.white,
    fontSize: 20,
  },
  buttonContainer: {
    backgroundColor: Colors.transparent,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginBottom: 10,
  },
  chatContainer: {
    backgroundColor: Colors.transparent,
    flex: 1,
    maxHeight: 300,
  },
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  fullscreen: {
    height: '100%',
    width: '100%',
  },
  overlayContainer: {
    backgroundColor: Colors.transparent,
    height: Layout.window.height,
    justifyContent: 'flex-end',
    position: 'absolute',
    width: Layout.window.width,
  },
});

type Props = {
  isBroadcast: boolean,
  channelId: string
}

const AgoraVideo = (props: Props) => {
  const { isBroadcast, channelId } = props;
  const [streamId, setStreamId] = useState<string>('');
  const {
    agoraEngine, hasJoined, broadcasterVideoState,
  } = useStream(isBroadcast, channelId);

  const [addStream] = useAddStream();

  const [stopStream] = useStopStream();

  const getStream = useImperativeGetStream();

  useEffect(() => {
    if (!channelId) return;

    (async () => {
      if (isBroadcast) {
        const { data } = await addStream({ variables: { streamSet: { channelName: channelId } } });
        setStreamId(data?.stream?._id || '');
      } else {
        const { data } = await getStream({ filter: { channelName: channelId, finished: false } });
        console.log({ data });
        setStreamId(data?.stream?._id || '');
      }
    })();
  }, [isBroadcast, channelId]);

  const navigation = useNavigation();

  const handleLeave = () => {
    navigation.goBack();
    if (isBroadcast) {
      stopStream({
        variables: {
          filter: { _id: streamId },
          streamSet: { finishedAt: new Date().toISOString() },
        },
      });
    }
  };

  const handleSwitchCamera = async () => {
    await agoraEngine?.switchCamera();
  };

  const renderLocal = () => (
    <RtcLocalView.SurfaceView
      style={styles.fullscreen}
      channelId={channelId}
      renderMode={VideoRenderMode.Hidden}
    />
  );

  const renderRemote = () => broadcasterVideoState === VideoRemoteState.Decoding ? (
    <RtcRemoteView.SurfaceView
      uid={1337}
      style={styles.fullscreen}
      channelId={channelId}
      renderMode={VideoRenderMode.Hidden}
    />
  ) : (
    <View style={styles.broadcasterVideoStateMessage}>
      <Text style={styles.broadcasterVideoStateMessageText}>
        {videoStateMessage(broadcasterVideoState)}
      </Text>
    </View>
  );

  return (
    <View style={styles.container}>
      {!hasJoined ? (
        <>
          <ActivityIndicator
            size={60}
            color={Colors.primary}
          />
          <Text>Joining Stream, Please Wait</Text>
        </>
      ) : (
        <>
          {isBroadcast ? renderLocal() : renderRemote()}
          <View style={styles.overlayContainer}>
            <View style={styles.chatContainer}>
              {Boolean(streamId) && <CommentListing streamId={streamId}/>}
            </View>
            <View style={styles.buttonContainer}>
              {isBroadcast
              && <Button
                compact
                title="Switch Camera"
                onPress={handleSwitchCamera}
              />}
              <Button compact title="Leave" onPress={handleLeave}/>
            </View>
          </View>
          <KeepAwake/>
        </>
      )}
    </View>
  );
};

export default AgoraVideo;
