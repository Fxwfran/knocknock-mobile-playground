import { gql } from '@apollo/client';
import { RouteProp } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, View } from 'react-native';

import client from '../apollo/client';
import VideoPlayback from '../components/organisms/VideoPlayback';
import { RootStackParamList } from '../types';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    width: '100%',
  },
});

type VideoPlaybackScreenRouteProp = RouteProp<RootStackParamList, 'VideoPlayback'>;

type Props = {
  route: VideoPlaybackScreenRouteProp
};

const VideoPlaybackScreen = (props: Props) => {
  const { route } = props;
  

  const stream = client.readFragment({
    id: `Stream:${route.params.streamId}`,
    fragment: gql`
      fragment PlaybackStream on Stream {
          _id
          startedAt
          finishedAt
          title
          videoUrl
      }
    `,
  });

  console.log({ params: route.params });
  console.log({ stream });

  return (
    <View style={styles.container}>
      <VideoPlayback stream={stream}/>
    </View>
  );
};

export default VideoPlaybackScreen;
