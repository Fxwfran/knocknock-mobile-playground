import { gql, useQuery } from '@apollo/client';
import React from 'react';
import { StyleSheet, View } from 'react-native';

import ChatRoomIntro from '../components/organisms/ChatRoomIntro';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});

const SCREN_QUERY = gql`
  query LoginChatQuery {
    streams {
      _id
      title
    }
  }
`;

const LoginChatScreen = () => {
  const { data, error } = useQuery(SCREN_QUERY);
  console.log({ data, error });
  return (
    <View style={styles.container}>
      <ChatRoomIntro/>
    </View>
  );
};

export default LoginChatScreen;
