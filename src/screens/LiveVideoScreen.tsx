import { RouteProp } from '@react-navigation/native';
import React from 'react';
import { StyleSheet } from 'react-native';

import AgoraVideo from '../components/organisms/AgoraVideo';
import View from '../components/organisms/View';
import Layout from '../constants/Layout';
import { RootStackParamList } from '../types';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: Layout.window.width,
  },
});

type LiveVideoScreenRouteProp = RouteProp<RootStackParamList, 'LiveVideo'>;

type Props = {
  route: LiveVideoScreenRouteProp
};

const LiveVideoScreen = ({ route }: Props) => (
  <View style={styles.container}>
    <AgoraVideo isBroadcast={route.params.isBroadcast} channelId={route.params.channelId}/>
  </View>
);

export default LiveVideoScreen;
