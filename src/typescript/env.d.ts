declare module '@env' {
  export const AGORA_APP_ID: string | undefined;
  export const APOLLO_URI: string | undefined;
}
