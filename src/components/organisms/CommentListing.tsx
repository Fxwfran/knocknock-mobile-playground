/* eslint-disable react-native/no-color-literals */
import { NetworkStatus } from '@apollo/client';
import React, { useRef, useState } from 'react';
import {
  FlatList,
  ListRenderItemInfo,
  NativeScrollEvent,
  NativeSyntheticEvent,
  StyleSheet,
  TextInput,
} from 'react-native';

import {
  useAddStreamComment,
  useGetStreamComments,
  useSubscribeStreamComments,
} from '../../apollo/hooks/streamComments';
import Colors from '../../constants/Colors';
import { StreamComment } from '../../generated/graphql';
import Text from '../atoms/Text';
import View from './View';

const styles = StyleSheet.create({
  avatar: {
    borderRadius: 50,
    height: 38,
    marginRight: 16,
    overflow: 'hidden',
    width: 38,
  },
  avatarContent: {
    fontSize: 30,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  bottomContainer: {
    alignItems: 'center',
    backgroundColor: Colors.transparent,
    flex: 0,
    flexDirection: 'row',
    padding: 16,
    width: '100%',
  },
  container: {
    alignItems: 'center',
    backgroundColor: Colors.transparent,
    flex: 1,
    width: '100%',
  },
  messageContainer: {
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: Colors.blackOpacity,
    borderRadius: 4,
    flexDirection: 'row',
    marginTop: 16,
    maxWidth: '100%',
    padding: 8,
  },
  messageContent: {
    backgroundColor: Colors.transparent,
    flexShrink: 1,
  },
  messageList: {
    marginBottom: 10,
  },
  messageListContainer: {
    alignItems: 'flex-start',
  },
  messageText: {
    color: Colors.white,
    flexShrink: 1,
  },
  textInput: {
    backgroundColor: Colors.blackOpacity,
    borderRadius: 4,
    color: Colors.white,
    flex: 1,
    padding: 16,
  },
  topContainer: {
    backgroundColor: Colors.transparent,
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 16,
    width: '100%',
  },
});

type Props = {
  streamId: string,
  canAddComment?: boolean,
}

const isScrollCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }: NativeScrollEvent) => layoutMeasurement.height + contentOffset.y >= contentSize.height - 20;

const CommentListing = (props: Props) => {
  const {
    streamId,
    canAddComment = true,
  } = props;
  const listRef = useRef<FlatList<StreamComment>>(null);
  const [messageBody, setMessageBody] = useState('');
  const [streamComments, setStreamComments] = useState<StreamComment[]>([]);
  const [isScrolled, setIsScrolled] = useState(false);
  const [addStreamComment] = useAddStreamComment({
    onCompleted: () => listRef.current?.scrollToEnd(),
  });

  useSubscribeStreamComments({
    variables: { streamId },
    onSubscriptionData: ({ subscriptionData }) => {
      if (subscriptionData.data?.streamComments) {
        setStreamComments([...streamComments, subscriptionData.data?.streamComments]);
      }
    },
  });

  const { refetch, networkStatus } = useGetStreamComments({
    variables: {
      filter: { streamId },
    },
    fetchPolicy: 'cache-and-network',
    onCompleted: (response) => {
      if (response.streamComments) {
        setStreamComments(response.streamComments);
      }
    },
    notifyOnNetworkStatusChange: true,
  });

  const submitComment = () => {
    addStreamComment({
      variables: {
        set: {
          messageBody,
          streamId,
        },
      },
    });
    setMessageBody('');
  };

  const handleCommentAddedToList = () => {
    if (!isScrolled) {
      listRef.current?.scrollToEnd();
    }
  };

  const handleScrollMessageList = (e: NativeSyntheticEvent<NativeScrollEvent>) => {
    setIsScrolled(!isScrollCloseToBottom(e.nativeEvent));
  };

  const renderItem = ({ item }: ListRenderItemInfo<StreamComment>) => (
    <View key={item.createdAt} style={styles.messageContainer}>
      <View style={styles.avatar}>
        <Text style={styles.avatarContent}>{item._id}</Text>
      </View>
      <View style={styles.messageContent}>
        <Text style={styles.messageText}>{item.messageBody}</Text>
      </View>
    </View>
  );

  return (
    <View style={styles.container}>
      <View style={styles.topContainer}>
        <FlatList
          contentContainerStyle={styles.messageListContainer}
          style={styles.messageList}
          ref={listRef}
          data={streamComments ?? []}
          renderItem={renderItem}
          onRefresh={refetch}
          refreshing={networkStatus === NetworkStatus.refetch}
          keyExtractor={(comment) => comment._id}
          onContentSizeChange={handleCommentAddedToList}
          onScroll={handleScrollMessageList}
        />
      </View>
      {canAddComment
      && <View style={styles.bottomContainer}>
        <TextInput
          style={styles.textInput}
          value={messageBody}
          onChangeText={setMessageBody}
          onSubmitEditing={submitComment}
          returnKeyType="send"
          enablesReturnKeyAutomatically={true}
          placeholder="Type your message here..."
          placeholderTextColor="#ffffff7f"
        />
      </View>}
    </View>
  );
};

export default CommentListing;
