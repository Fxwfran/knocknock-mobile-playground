import { gql, useQuery } from '@apollo/client';
import React from 'react';
import { StyleSheet, View } from 'react-native';

import AgoraChannelSelection from '../components/organisms/AgoraChannelSelection';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});

const SCREN_QUERY = gql`
  query ChannelSelectionQuery {
    streams {
      _id
      title
    }
  }
`;

const ChannelSelectionScreen = () => {
  const { data, error } = useQuery(SCREN_QUERY);
  console.log({ data, error });
  return (
    <View style={styles.container}>
      <AgoraChannelSelection/>
    </View>
  );
};

export default ChannelSelectionScreen;
