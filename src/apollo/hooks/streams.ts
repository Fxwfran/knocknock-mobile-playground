import {
  gql,
  LazyQueryHookOptions,
  MutationHookOptions,
  QueryHookOptions,
  useMutation,
  useQuery,
} from '@apollo/client';

import { Stream, StreamFilter } from '../../generated/graphql';
import { GenericFilter } from '../types';
import { useImperativeQuery } from '../utils';

interface IGetStreams {
  streams: Stream[]
}

interface IGetStream {
  stream: Stream
}

export const useGetStreams = (options?: QueryHookOptions<IGetStreams, GenericFilter<StreamFilter>>) => useQuery<IGetStreams, GenericFilter<StreamFilter>>(gql`
    query GetStreams($filter: StreamFilter) {
        streams(filter: $filter) {
            _id
            startedAt
            finishedAt
            title
            videoUrl
        }
    }
`, options);

export const useImperativeGetStream = (options?: LazyQueryHookOptions<IGetStream, GenericFilter<StreamFilter>>) => useImperativeQuery<IGetStream, GenericFilter<StreamFilter>>(gql`
    query ImperativeGetStream($filter: StreamFilter) {
        stream(filter: $filter) {
            _id
            startedAt
            finishedAt
            title
        }
    }
`, options);

export const useAddStream = (options?: MutationHookOptions) => useMutation<IGetStream>(gql`
    mutation AddStream($streamSet: StreamUpdate) {
        stream(set: $streamSet) {
            _id
        }
    }
`, options);

export const useStopStream = (options?: MutationHookOptions) => useMutation<IGetStream>(gql`
    mutation StopStream($filter: StreamFilter, $streamSet: StreamUpdate) {
        stream(filter: $filter, set: $streamSet) {
            _id
        }
    }
`, options);
