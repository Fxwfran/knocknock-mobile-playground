import React, { useState } from 'react';
import {
  ActivityIndicator, KeyboardAvoidingView, Platform, StyleSheet, TouchableOpacity, View,
} from 'react-native';
import { Icon } from 'react-native-elements';
import Video from 'react-native-video';

import Colors from '../../constants/Colors';
import Layout from '../../constants/Layout';
import { Stream } from '../../generated/graphql';
import CommentListing from './CommentListing';

const styles = StyleSheet.create({
  backgroundVideo: {
    bottom: 0,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
  },
  chatContainer: {
    backgroundColor: Colors.transparent,
    flex: 1,
    maxHeight: 300,
  },
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    width: '100%',
  },
  controls: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '100%',
  },
  overlay: {
    bottom: 0,
    height: Layout.window.height,
    justifyContent: 'flex-end',
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    width: Layout.window.width,
    zIndex: 2,
  },
});

const UrlPrefix = 'https://themes-icons.s3.amazonaws.com/';

type Props = {
  stream: Stream
};

const VideoPlayback = (props: Props) => {
  const { stream } = props;

  const [loading, setLoading] = useState<boolean>(true);
  const [paused, setPaused] = useState<boolean>(false);
  const [muted, setMuted] = useState<boolean>(false);

  const togglePause = () => {
    setPaused((prev) => !prev);
  };

  const toggleMute = () => {
    setMuted((prev) => !prev);
  };

  return (
    <View style={styles.container}>
      {loading
        ? <ActivityIndicator size={60} color={Colors.primary}/>
        : <KeyboardAvoidingView
          style={styles.overlay}
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        >
          <View style={styles.chatContainer}>
            <CommentListing streamId={stream._id}/>
          </View>
          <View style={styles.controls}>
            <TouchableOpacity onPress={togglePause}>
              {paused
                ? <Icon name="play-arrow" color="#fff" size={50}/>
                : <Icon name="pause" color="#fff" size={50}/>
              }
            </TouchableOpacity>
            <TouchableOpacity onPress={toggleMute}>
              {muted
                ? <Icon name="volume-off" color="#fff" size={50}/>
                : <Icon name="volume-up" color="#fff" size={50}/>
              }
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      }
      <Video
        source={{ uri: UrlPrefix + stream?.videoUrl }}
        onReadyForDisplay={() => setLoading(false)}
        onEnd={() => console.log('Stream playback ended.')}
        paused={paused}
        muted={muted}
        resizeMode="cover"
        style={styles.backgroundVideo}
      />
    </View>
  );
};

export default VideoPlayback;
