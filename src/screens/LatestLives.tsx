import React from 'react';
import { SafeAreaView, StatusBar, StyleSheet } from 'react-native';

import { useGetStreams } from '../apollo/hooks/streams';
import LatestLivesViewer from '../components/molecules/LatestLivesViewer';

const LatestLives = () => {
  const { data, refetch, networkStatus } = useGetStreams({
    notifyOnNetworkStatusChange: true,
  });

  const handleRefetchData = () => {
    console.log('Refetching...');
    refetch();
  };

  return (
    <SafeAreaView style={styles.container}>
      <LatestLivesViewer
        streams={data?.streams ?? []}
        refetchStatus={networkStatus}
        onRefetchData={handleRefetchData}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    marginTop: StatusBar.currentHeight || 0,
  },
});

export default LatestLives;
