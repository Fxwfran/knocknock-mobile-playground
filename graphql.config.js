module.exports = {
  schema: ['schema.graphql', 'schema-client.graphql'],
  extensions: {
    codegen:
    {
      generates: {
        './src/types.ts': {
          plugins: ['typescript', 'typescript-resolvers'],
        },
      },
    },
  },
};
