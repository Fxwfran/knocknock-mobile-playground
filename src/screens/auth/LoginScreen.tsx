import { RouteProp, useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { StyleSheet } from 'react-native';

import { useImperativeGetAccessToken } from '../../apollo/hooks/user';
import { accessTokenVar } from '../../apollo/state';
import Button from '../../components/atoms/Button';
import Text from '../../components/atoms/Text';
import TextInput from '../../components/atoms/TextInput';
import View from '../../components/organisms/View';
import { RootStackParamList } from '../../types';

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  phoneInput: {
    height: 50,
    marginBottom: 20,
    marginTop: 100,
  },
});

interface Props {
  route: RouteProp<RootStackParamList, 'Login'>;
}
const LoginScreen = (props: Props) => {
  const { phoneNumber } = props.route.params;
  const [phoneCode, setPhoneCode] = useState('');
  const getAccessToken = useImperativeGetAccessToken();
  const navigation = useNavigation();

  const submit = async () => {
    const { data: { accessToken } } = await getAccessToken({ filter: { phoneNumber, phoneCode: parseInt(phoneCode, 10) } });
    accessTokenVar(accessToken);
    console.log('Saved accessToken', accessToken);
    navigation.navigate('ScreenSelector');
  };

  return (
    <View style={styles.container}>
      <Text>We sent you a code!</Text>
      <TextInput
        style={styles.phoneInput}
        value={phoneCode}
        onChangeText={setPhoneCode}
        textContentType="oneTimeCode"
      />
      <Button onPress={submit} title="Send code"></Button>
    </View>
  );
};

export default LoginScreen;
