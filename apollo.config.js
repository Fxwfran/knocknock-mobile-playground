module.exports = {
  client: {
    service: {
      name: 'knocknock-tv',
      url: 'http://localhost:4000/graphql',
    },
    includes: ['schema-client.graphql'],
  },
};
