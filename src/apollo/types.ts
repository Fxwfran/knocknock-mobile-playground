export interface GenericFilter<T = any> {
  filter: T
}
