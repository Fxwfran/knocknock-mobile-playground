import React from 'react';
import { TextInput as DefaultTextInput } from 'react-native';

import Colors from '../../constants/Colors';
import { useThemeColor } from '../../hooks/useThemeColor';

type Props = {
  fullWidth?: boolean
} & ThemeProps & DefaultTextInput['props'];

const TextInput = (props: Props) => {
  const {
    style, lightColor, darkColor, fullWidth, ...otherProps
  } = props;
  const borderColor = Colors.primary;
  const color = useThemeColor({ light: lightColor, dark: darkColor }, 'text');

  return <DefaultTextInput
    style={[{ borderWidth: 1, borderColor, color }, fullWidth && { alignSelf: 'stretch' }, style]}
    underlineColorAndroid="transparent"
    placeholderTextColor={`${color}7F`}
    {...otherProps}
  />;
};

export default TextInput;
