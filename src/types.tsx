export type RootStackParamList = {
  ScreenSelector: undefined;
  ChannelSelection: undefined;
  LatestLives: undefined;
  LiveVideo: { isBroadcast: boolean, channelId: string };
  NotFound: undefined,
  ChatStream: {streamId: string}
  ChatIntroRoom: undefined
  GetPhoneCode: undefined
  Login: { phoneNumber: string }
  VideoPlayback: { streamId: string };
};
