import { gql, useQuery } from '@apollo/client';
import { useNavigation } from '@react-navigation/native';
import React from 'react';
import {
  FlatList, SafeAreaView, StyleSheet, Text,
} from 'react-native';
import { ListItem } from 'react-native-elements';

import { accessTokenVar } from '../apollo/state';
import Button from '../components/atoms/Button';

const styles = StyleSheet.create({
  container: {
    alignContent: 'center',
    flex: 1,
    justifyContent: 'center',
    width: '100%',
  },
});

const SCREEN_SELECTION_QUERY = gql`
  query ScreenSelector {
    isLoggedIn @client
  }
`;

const ScreenSelector = () => {
  const navigation = useNavigation();
  const { data: { isLoggedIn } } = useQuery(SCREEN_SELECTION_QUERY);
  console.log(isLoggedIn);
  const screenList = [
    'LatestLives',
    'ChannelSelection',
    'ChatIntroRoom',
    ...(isLoggedIn ? [] : ['GetPhoneCode']),
  ];

  const handlePress = (screen: string) => {
    navigation.navigate(screen);
  };

  const logout = () => {
    accessTokenVar('');
  };

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={screenList}
        keyExtractor={(item) => item}
        renderItem={({ item }) => (
          <ListItem bottomDivider onPress={() => handlePress(item)}>
            <ListItem.Content>
              <Text>{item}</Text>
            </ListItem.Content>
          </ListItem>
        )}
      />
      {Boolean(isLoggedIn) && <Button title="Log out" onPress={logout}></Button>}
    </SafeAreaView>
  );
};

export default ScreenSelector;
