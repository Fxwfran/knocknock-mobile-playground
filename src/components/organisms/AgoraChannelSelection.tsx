import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';

import Button from '../atoms/Button';
import TextInput from '../atoms/TextInput';

const styles = StyleSheet.create({
  bottomSpace: {
    marginBottom: 10,
  },
  container: {
    alignItems: 'stretch',
    flex: 1,
    justifyContent: 'center',
  },
});

const AgoraChannelSelection = () => {
  const navigation = useNavigation();

  const [channelId, setChannelId] = useState<string>('');

  const handleBroadcast = async () => {
    navigation.navigate('LiveVideo', { isBroadcast: true, channelId });
  };

  const handleJoin = async () => {
    navigation.navigate('LiveVideo', { isBroadcast: false, channelId });
  };

  return (
    <View style={styles.container}>
      <TextInput
        fullWidth
        placeholder="Channel name"
        onChangeText={setChannelId}
        defaultValue={channelId}
        style={styles.bottomSpace}
      />
      <Button
        style={styles.bottomSpace}
        title="Broadcast"
        onPress={handleBroadcast}
      />
      <Button
        style={styles.bottomSpace}
        title="Join"
        onPress={handleJoin}
      />
    </View>
  );
};

export default AgoraChannelSelection;
