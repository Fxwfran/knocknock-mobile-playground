import React from 'react';
import { Text as DefaultText } from 'react-native';

import { useThemeColor } from '../../hooks/useThemeColor';

type Props = ThemeProps & DefaultText['props'];

const Text = (props: Props) => {
  const {
    style, lightColor, darkColor, ...otherProps
  } = props;
  const color = useThemeColor({ light: lightColor, dark: darkColor }, 'text');

  return <DefaultText style={[{ color }, style]} {...otherProps} />;
};

export default Text;
