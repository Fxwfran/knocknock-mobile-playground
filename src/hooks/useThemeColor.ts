import Colors from '../constants/Colors';
import useColorScheme from './useColorScheme';

type Props = {
    light?: string,
    dark?: string
}

export const useThemeColor = (
  props: Props,
  colorName: keyof typeof Colors.light & keyof typeof Colors.dark,
) => {
  const theme = useColorScheme();
  const colorFromProps = props[theme];

  if (colorFromProps) {
    return colorFromProps;
  }
  return Colors[theme][colorName];
};
